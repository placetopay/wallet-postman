# Wallet Postman Workspace ![Backend Version](https://img.shields.io/badge/release-v0.74.0-blue)

Copy of the Postman workspace for Wallet.

## Getting Started

These instruction will give you a copy of the postman workspace up and running on your local machine.

### Prerequisites

- [Postman](https://www.postman.com/downloads/).
- An [Accounts](https://accounts-test.placetopay.com/) user.
- Client Id and Client Secret for Accounts, Users and Merchants.
- Json Language Feature (VSCode extension for format files downloaded from Postman)

__Note:__ Ask to your leader the missing data.

### Installing

First download this repository and unzip it in your local machine.

```
git clone git@bitbucket.org:placetopay/wallet-postman.git
```

__Note:__ You can downloaded directly from the browser, [here](https://bitbucket.org/placetopay/wallet-postman/downloads/?tab=tags).

Then open Postman and create a workspace with the name `Wallet`.

Inside of the workspace, click on File > Import > Folder and choose folder from your computer.

Finally select the unzip folder and click on Import.

## Usage

### Collection.

Collections are a set of endpoints for creating, editing, reading, and deleting data. Technically an endpoint is an HTTP request focused on a particular task. All the endpoints have its parameters, body and filter. The parameters

> There are some optional data in body to sent, it appears as `_data`, `_` means the data is not considered for the Backend.
#### Accounts

This is used to ask the token for Accounts, you need this token for Merchants.

#### Administrator

These resources are special for an adminsitrator user, who could see all the information of the wallet. This user also requires some roles and permissions to access each action.


#### Base

This includes all the request that does not required any authentication as read general data, ask otp, create device, etc.

#### Firebase Cloud Messaging

There are some endpoints used by the Backend to send the notifications to Firebase.

#### Merchants

Here you you get your Accounts user and you can create payment requests, exports, read notification, transactions and more.

#### Merchants -> Panel

With the Merchants credentials, you can use many endpoints from Wallet to Panel directly to get profiles, sites and merchants.

#### Site Credentials

This collection require the `site credentials`, it just can read a transaction of a site.

#### Users

Here you create a user to pay, create pockets, payment requests, contacts, exports, payment methods, read notification, transactions and more

### Enviroments.

Environments allows you to store data for use in collections, this data is stored safely so as not to expose sensitive data when sharing a collection or environment in particular.

__NOTE:__ All the variables with `manual` as initial value need to be filled first, because it is required or there is not optimization.

#### wallet dev-deployment.

This file saves all the data necessary to operate in collections; like save tokens, client ids, client secrets, urls, data from the authenticated user and some helpers to automate some endpoints.


#### custom environment.

Depends of the work you could duplicate `wallet dev` or `wallet test` collections in order to have more than one user o merchant, work in a local or another environment, etc.
